﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiveSuperpower : MonoBehaviour {

    public GameObject superpower;

    public void activated()
    {
        superpower.SetActive(true);
    }
}
