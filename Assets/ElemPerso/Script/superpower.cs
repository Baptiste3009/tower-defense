﻿using System.Collections;
using System.Collections.Generic;
using TowerDefense.Towers;
using TowerDefense.Level;
using UnityEngine;

public class superpower : MonoBehaviour {

    public bool isUse;
    public Animator animPower;
    public AudioSource hohoho;

    public void SuperPower()
    {
        if (isUse == false)
        {
            isUse = true;
            animPower.SetBool("isUse", isUse);
            StartCoroutine(SantaPower());
            hohoho.Play();
        }
    }

    IEnumerator SantaPower()
    {
        for (int i = 0; i < 5; i++)
        {
            LevelManager.instance.currency.AddCurrency(1);
            yield return new WaitForSeconds(2);
        }
        hohoho.Stop();
        yield return new WaitForSeconds(20);
        isUse = false;
        animPower.SetBool("isUse", isUse);
    }
}
