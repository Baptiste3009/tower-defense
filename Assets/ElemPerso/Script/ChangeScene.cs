﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class ChangeScene : MonoBehaviour {

    public string SceneTarget;

    public void Change()
    {
        SceneManager.LoadScene(SceneTarget);
    }

}
